#!/bin/bash

docker run \
    -v $(pwd):/tests \
    golang:1.16.6-alpine3.14 \
    scripts/test_in_docker.sh
