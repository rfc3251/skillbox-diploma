#Test comment
#FROM golang:1.16.6-alpine3.14 as builder
#RUN mkdir /build
#WORKDIR /build
#COPY . . /build/

##RUN go mod download
#RUN GO111MODULE=on CGO_ENABLED=0 GOOS=linux go build -o app cmd/server/app.go

ARG TAG=

FROM alpine:${TAG}

EXPOSE 8080
COPY app app
CMD ["./app"]
