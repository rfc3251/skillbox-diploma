Для деплоя сервиса необходимо:
1. Задать переменные в deploy.yml:  
TAG - версия образа Docker, в котором будет запущено приложение (можно оставить по умолчанию)  
project_path - каталог проекта, в который будет скачан репозиторий и откуда будет запущено приложение  
2. В файле hosts указать свои параметры подключения Ansible к ВМ, где будет хостится сервис (внешний IP адрес, пользователь).  
3. Задать переменную окружения GITLAB_SSH_PRIVATE_KEY, содержащую закрытый SSH ключ для подключения к ВМ, где будет хостится сервис.
4. Запустить плейбук – ansible-playbook deploy.yml.


# skillbox-diploma
Чтобы скопировать репозиторий к себе для работы, вам нужно следовать [этим инструкциям](https://docs.github.com/en/github/creating-cloning-and-archiving-repositories/creating-a-repository-on-github/duplicating-a-repository#mirroring-a-repository-in-another-location).

## Runtime
Приложение отвечает по 3 эндпоинтам:  
* /health - 200 ok
* /metrics - в формате метрик для prometheus, включая счётчик запросов в основной эндпоинт `skillbox_http_requests_total`
* / - основной эндпоинт, возвращающий часть запроса и генерирующий строчку лога.

## Как работать с приложением без docker:  
1. Установить golang 1.16
2. Установить зависимости:

   ```shell
   go mod download
   ```

3. Запустить тесты:
   ```shell
   go test -v ./...
   ```
4. Собрать приложение:
   ```
   GO111MODULE=on go build -o app cmd/server/app.go
   ```
5. Запустить его:
   ```shell
   ./app
   ```

## Как работать с приложением в docker:  
1. Установить docker
2. Запустить тесты
   ```shell
   ./run-tests.sh
   ```
3. Собрать:
   ```shell
   docker-compose build
   ```
   or
   ```shell
   docker build . -t skillbox/app
   ```
4. Запустить:
   ```shell
   docker-compose up
   ```
   or
   ```shell
   docker run -p8080:8080 skillbox/app
   ```
